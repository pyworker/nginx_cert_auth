# Аутентификация пользователей в Nginx с использованием SSL-сертификатами

> Зависимости python-пакетов: elasticsearch, requests

## Подготовка SSL-сертификатов

Для работы понадобится пара: ssl-сертификат, ключей для пользователя, сервера и центра сертификации (CA). В качестве центра сертификации выступает одна из локальных систем (обычно в качестве центра сертификации используют VeriSign, thawte, GoDaddy и т. Д.), которая подписывает сертификаты клиентов и серверов. 

1. Создание ключа СА и сертификата для подписания клиентских сертификатов

```bash
openssl genrsa -des3 -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
```

2. Создание ключа и сертификата для сервера. **Для server.key пароль не указывать**

```bash
openssl genrsa -des3 -out server.key 1024
openssl req -new -key server.key -out server.csr
openssl rsa -in server.key -out server.key
```

3. Подписываем сертификат сервера cозданным СА  

```bash
openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
```

4. Создание ключа и сертификата для клиента

```bash
openssl genrsa -des3 -out client.key 1024
openssl req -new -key client.key -out client.csr
openssl rsa -in client.key -out client.key
```

5. Подписываем сертификат клиента созданным СА  

```bash
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 02 -out client.crt
```

## Настройка Nginx на работу с сертификатами

1. Настраиваем Nginx на получение конфигурационных файлов из директории `site-enabled`. Для этого подготавливается конфигурационный файл `nginx.conf`, содержащий основную секцию `http`. Подготовленный конфигурационный файл `nginx.conf` монтируется внутрь контейнера к `:/etc/nginx/nginx.conf`.

```bash
# [project]/nginx/nginx.conf

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;

    keepalive_timeout  15;
    sendfile        on;

    include /etc/nginx/site-enabled/*.conf;
    include /etc/nginx/conf.d/*.conf;
}

```

2. Настроить подключение к elastic. Подготавливаем конфигурационный файл `cert_auth.conf`, который монтируется внутрь контейнера к `/etc/nginx/site-enabled/cert_auth.conf` 

```bash
# [project]/nginx/site-enabled/cert_auth.conf

upstream elasticsearch {
    server elasticsearch:9200;
}

server {
    listen 101;

    access_log      /var/log/nginx/cert_auth.access.log combined;
    error_log       /var/log/nginx/cert_auth.error.log warn;
        
    location / {
        proxy_pass http://elasticsearch;
        proxy_redirect off;
        proxy_buffering off;
        proxy_http_version 1.1;
    }
}

server {
    listen      4433;
    ssl         on;

    access_log          /var/log/nginx/cert_auth.access.log combined;
    error_log           /var/log/nginx/cert_auth.error.log warn;

    ssl_certificate         /etc/nginx/certs/server.crt;
    ssl_certificate_key     /etc/nginx/certs/server.key;
    ssl_client_certificate  /etc/nginx/certs/ca.crt;
    ssl_verify_client       on;

    location / {
        proxy_pass http://elasticsearch;
        proxy_redirect off;
        proxy_buffering off;
        proxy_http_version 1.1;
    }
}
```

Для работы с ssl-сертификатами в настройках Nginx требуется указать: 

- серверный сертификат (server.crt)
- закрытый серверный ключ (server.key)
- сертификат центра сертификации который используется для подписания клиентских сертификатов (ca.crt).

Параметр `ssl_verify_client` может принимать следующие значения: 

- `on` - в этом случае Nginx требует от клиента сертификата (без клиентского сертификата соединение установлено не будет). 
- ` optional` - в этом случае клиентский сертификат проверяется опционально (соединение может быть установлено без клиентского сертификата).

## Подготовка и запуск стенда с использованием контейнерной виртуализации Docker 

Для запуска стенда со всеми настройками подготавливается `docker-compose` файл со следующим содержанием.

```
# [project]/docker/docker-compose.yml 

version: '3.4'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.2.1
    deploy:
      mode: global
    environment:
      - 'ES_JAVA_OPTS=-Xmx256m -Xms256m'
      # - bootstrap.memory_lock=true
      - 'xpack.security.enabled=false'
      - 'xpack.watcher.enabled=false'
    restart: always

  web:
    image: nginx
    deploy:
      mode: global
    depends_on:
      - elasticsearch
    links:
      - 'elasticsearch:elasticsearch'
    ports:
      - '5588:101'
      - '4433:4433'
    environment:
      - NGINX_PORT=${NGINX_PORT}
      - NGINX_HOST=${NGINX_HOST}
    volumes:
      - /home/pyworker/nginx_cert_auth/nginx/nginx.conf:/etc/nginx/nginx.conf
      - /home/pyworker/nginx_cert_auth/nginx/site-available:/etc/nginx/site-enabled/ 
      - /home/pyworker/nginx_cert_auth/nginx/certs:/etc/nginx/certs/

    command: [nginx-debug, '-g', 'daemon off;']
    restart: always
```

Данный файл запускает два контейнера: с `elasticsearch` и `nginx`. Контейнер с `nginx` запускается после контейнера с `elasticsearch`.

Запуск стенда осуществляется в директории, в которой подготовлен `docker-compose` файл (все команды, к docker-compose выполняются из директории с docker-compose.yml файлом). Для запуска стенда выполняется следующая команда:

```bash
$ docker-compose up -d
```

Проверить состояние стенда можно командой

```bash
$ docker-compose ps 
```

Перезапуск отдельного контейнера стенда осуществляется командой

```
$ docker-compose restart <container_name>
```

Остановка работы стенда осуществляется командой

```bash
$ docker-compose stop
```

Удаление всех остановленных контейнеров стенда осуществляется командой 

```bash
$ docker-compose rm
```

## REST клиент (CURL)

Для проверки корректности настроек после запуска стенда необходимо 

1. Из директории, в которой располагается клиентские сертификат и ключ, выполнить команду `curl` со следующим набором флагов:
-v (verbose )
-s (silent flags) 
-k для работы с самоподписанный сертификатами сервера.
```bash
$ curl -v -s -k --key client.key --cert client.crt https://localhost:4433
> GET / HTTP/1.1
> Host: localhost:4433
> User-Agent: curl/7.55.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: nginx/1.13.9
< Date: Thu, 29 Mar 2018 08:41:16 GMT
< Content-Type: application/json; charset=UTF-8
< Content-Length: 436
< Connection: keep-alive
< 
{
  "name" : "FHgNi7y",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "P-4dpiChSXChp8mb0kmmFw",
  "version" : {
    "number" : "6.2.1",
    "build_hash" : "7299dc3",
    "build_date" : "2018-02-07T19:34:26.990113Z",
    "build_snapshot" : false,
    "lucene_version" : "7.2.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
* Connection #0 to host localhost left intact
```

Для проверки работы без ssl сертификатов выполняется следующая команда 
```
$ curl -v -k https://127.0.0.1:5588
> GET / HTTP/1.1
> Host: localhost:4433
> User-Agent: curl/7.55.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: nginx/1.13.9
< Date: Thu, 29 Mar 2018 08:41:16 GMT
< Content-Type: application/json; charset=UTF-8
< Content-Length: 436
< Connection: keep-alive
< 
{
  "name" : "FHgNi7y",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "P-4dpiChSXChp8mb0kmmFw",
  "version" : {
    "number" : "6.2.1",
    "build_hash" : "7299dc3",
    "build_date" : "2018-02-07T19:34:26.990113Z",
    "build_snapshot" : false,
    "lucene_version" : "7.2.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
* Connection #0 to host localhost left intact
```


## REST клиент (PYTHON)

Для использования самоподписанных сертификатов можно использовать стандартную библиотеку `requests`. Пример использования приведен ниже

```python
def request_call():
    """
    return response from server
    Return:
        data from server response 
    """
    r = requests.get(
    url='https://localhost:4433',
    # certificate is self-signed dont need to verify it 
    verify=False,
    # pass to certificate and key
    cert=(
        '/home/pol4/Projects/lab/nginx_cert_auth/nginx/certs/client.crt',
        '/home/pol4/Projects/lab/nginx_cert_auth/nginx/certs/client.key'))
    return r.text

def main():
    """
    main logic: test auth in elastic
    Return:
        True: if func done
    """
    print('response from server: %s' % request_call())
    return True

if __name__ == '__main__':
    main()
```

Для взаимодействия с elasticsearch может быть использована библиотека `elasticsearch`, в которой явно указывается, что для авторизации пользователей используются сертификаты.

```python

# [project]/bin/elastic_cluster.py

import ssl
from elastic_conf import HOSTS, CLIENT_CRT, CLIENT_KEY
from elasticsearch import Elasticsearch, TransportError, 


class ElasticCluster:
    def __init__(self, index, type):
        self._index = index
        self._type = type

        # create ssl context
        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain(
                    certfile=CLIENT_CRT,
                    keyfile=CLIENT_KEY)
        try:
            self._es = Elasticsearch(
                hosts=HOSTS,
                ssl_context=context,
                use_ssl=True,
                scheme="https", 
                verify_certs=False,)
            self._es.info()
        except TransportError as transport_err:
            # if HOSTS not for elastic
            raise ConnectionError('failed connect to elastic hosts: "%s" error: "%s"' % (HOSTS, transport_err.error))

    def add(self):
        .   .   .

    def update(self):
        .   .   .

```


## Ошибки
failed (SSL: error:0906406D:PEM)

При создании приватного ключа, был использован пароль. Чтобы перезагрузить nginx, нужно удалить пароль из приватного ключа.

```bash
openssl rsa -in /etc/nginx/certs/server.key -out /etc/nginx/certs/server.key
```


## Дополнение 
You can do that in one command:
```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365
```
You can also add -nodes if you don't want to protect your private key with a passphrase, otherwise it will prompt you for "at least a 4 character" password.


# Руководство по запуску стенда "Безопасность"

При необходимости подготовить swarm кластер

```bash
$ docker swarm init --advertise-addr 192.168.99.100
Swarm initialized: current node (dxn1zf6l61qsb1josjja83ngz) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.99.100:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Для проверки состояния swarm кластера выполняется команда (список нод)

```
$ docker node ls

ID                           HOSTNAME  STATUS  AVAILABILITY  MANAGER STATUS
dxn1zf6l61qsb1josjja83ngz *  manager1  Ready   Active        Leader
```

Получить проект с репозитория `git clone https://pyworker@bitbucket.org/pyworker/nginx_cert_auth.git`. И перейти в директорию с проектом.

```bash
# in [project]/nginx/site-available
nano cert_auth.conf # change client_cert_verify from on to optional

# in [project]/docker change config and deploy application
nano docker-compose.yml # change path (in section "volumes")
docker stack deploy -c docker-compose.yml ngd

# check application status
docker stack ps ngd

# in [project]/nginx/certs/ check service availability by http and https
curl -v http://83.149.218.204:5588
curl -v -s -k --key ./client.key --cert ./client.crt https://83.149.218.204:4433

```

при попытке установить соединение по https может возникать следующая ошибка 
```
* Connected to 83.149.218.204 (83.149.218.204) port 4433 (#0)
* Initializing NSS with certpath: sql:/etc/pki/nssdb
* NSS error -8054 (SEC_ERROR_REUSED_ISSUER_AND_SERIAL)
* You are attempting to import a cert with the same issuer/serial as an existing cert, but that is not the same cert.
* Closing connection 0
```

ошибка чинится УДАЛЕНИЕМ существующих сертификатов и подписью новых, при этом при выполнении команды `openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 02 -out client.crt`
*важно обратить внимание* на `-set_serial` он должен содержать уникальный параметр. *Два сертификата с одинаковыми серийными номерами существовать не могут*
(пароль для центра сертификации 1012)
