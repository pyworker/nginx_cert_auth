import json
from elastic_cluster import ElasticCluster
from elastic_conf import CLIENT_CRT, CLIENT_KEY
import requests

def create_document():
    """
    prepare document for elastic
    """
    return json.dumps({
        'name': 'bob',
        'sername': 'momo'
    })


def search_document():
    """
    create search query to elastic
    """
    return json.dumps({
        'version': True,
        'query': {
            'term': {'name': 'bob'}}})


def request_call():
    """
    return response from server
    Return:
        data from server response 
    """
    r = requests.get(
    url='https://localhost:4433',
    verify=False,
    cert=(CLIENT_CRT, CLIENT_KEY))
    return r.text


def main():
    """
    main logic: test auth in elastic
    """
    print('response from server: %s' % request_call())

    el = ElasticCluster(index='test_auth', type='auth')
    document = create_document()
    query = search_document()
    try:
        pass
        el.add(document=document)
        print(el.find(search_query=query))
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()